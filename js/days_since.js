Date.sinceIncident = function( incident_date, datenow ) {

//how many milliseconds in a day    
var one_day=1000*60*60*24;

//convert two dates to milliseconds
var incident_datems = incident_date.getTime();
var datenowms = datenow.getTime();

//get difference in milliseconds
var date_difference = datenowms - incident_datems;

//convert back to days
return Math.round(date_difference/one_day);
}

//set the last incident date
var incident = new Date(2019, 4, 3);
//set date today
var today = new Date();

//static text header
document.getElementById("headertext").innerHTML = "<span class='headerline'>We practice zero major incident culture here.</span>";

//static text 1
document.getElementById("statictext").innerHTML = "<span class='text'>days since last reported major incident</span>";

//display days difference
document.getElementById("timer").innerHTML = Date.sinceIncident(incident, today);


console.log( 'Days since the last incident which occurred last ' 
           + incident.toLocaleDateString() + ': ' 
           + Date.sinceIncident(incident, today));